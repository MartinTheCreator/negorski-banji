---
title: Контакт
slug: kontakt
form:
    name: contact-form

    fields:
        - name: name
          placeholder: Вашето име
          autocomplete: on
          type: text
          outerclasses: input-wrapper
          validate:
            required: true

        - name: email
          placeholder: Вашиот email
          type: email
          outerclasses: input-wrapper
          validate:
            required: true

        - name: message
          placeholder: Порака
          type: textarea
          outerclasses: textarea-wrapper
          validate:
            required: true

    buttons:
        - type: submit
          value: "<span>Испрати</span>"
          outerclasses: button-wrapper
          classes: btn btn-gradient

    process:
        - email:
            subject: "[Negorski Banji Contact Form] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: contact-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Ви благодариме за пораката!
        - display: /contact
---